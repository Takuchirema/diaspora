# Load the rails application
require_relative 'application'

# Initialize the rails application
Diaspora::Application.initialize!

Diaspora::Application.config.session_store :cookie_store, key: '_testapp_session', :domain => :all, :httponly => false
