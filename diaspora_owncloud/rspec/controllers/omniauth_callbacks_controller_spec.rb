class OmniauthCallbacksController < Devise::OmniauthCallbacksController
  
  def facebook
    require 'koala'

    session["devise.facebook_data"] = request.env["omniauth.auth"]
    puts "user data is "+(request.env["omniauth.auth"].inspect)
    
    @graph = Koala::Facebook::API.new(request.env["omniauth.auth"].credentials.token)
    friends = @graph.get_connections("me", "friends",api_version: 'v2.0',:fields => "name, id, education,email")
    
    current_user.update(fb_id:request.env["omniauth.auth"].extra.raw_info.id.to_s)
    
    puts "##########friends length "+friends.length.to_s
    
    #check if Facebook aspect is already there
    if current_user.aspects.where(:name => "Facebook").blank?
      puts " $$$$$$$ aspect new "
      @aspect = current_user.aspects.build('name' => 'Facebook' , "contacts_visible"=>true)
      puts " $$$$$$$ aspect new "
      
      if @aspect.save
        
        friends.each { |x| 
          puts x['name']
          add_aspect_friend(x['id'],@aspect)
        }
        
        redirect_to session[:diaspora_handle]+"/contacts?a_id="+(@aspect.id.to_s)
      else
        render nothing: true, status: 422
      end
    else
      
      @aspect = current_user.aspects.where(:name => "Facebook").first
      puts " $$$$$$$ aspect already in! "+@aspect.inspect
      
      friends.each { |x| 
        puts x['name']
        add_aspect_friend(x['id'],@aspect)
      }
      
      puts " %%%%%%%%%%%%%%%%%%%%%% redirecting to "+session[:diaspora_url]+"contacts?a_id="+(@aspect.id.to_s)
      redirect_to session[:diaspora_url]+"contacts?a_id="+(@aspect.id.to_s)
    end

  end
  
  def add_aspect_friend (id,aspect)
    
    @user = User.where(:fb_id => id).first
    
    if (@user != nil)
      puts " -------- after user 1-- "+@user.username
      @person = Person.where(:owner_id => @user.id).first
      puts " -------- after user 2-- "+@user.id.to_s+" "+@person.inspect
      
      @aspect = aspect
      
      begin
        @contact = current_user.share_with(@person, @aspect)

        if @contact.present?
          flash.now[:notice] = I18n.t("aspects.add_to_aspect.success")
          puts "asect add success "+id
        else
          flash.now[:error] = I18n.t("aspects.add_to_aspect.failure")
          puts "asect add failure "+id
        end
        
      rescue
        flash.now[:error] = I18n.t("aspects.add_to_aspect.failure")
        puts "user already shared"
      end
      
    else
      puts "------------------- user is null"
    end
  end

  def failure
    redirect_to root_path
  end
  
end
