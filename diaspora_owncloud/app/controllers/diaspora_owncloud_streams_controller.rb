class DiasporaOwncloudStreamsController < ::StreamsController
  
  before_action :authenticate_user!
  before_action :save_selected_aspects, :only => :aspects

  layout proc { request.format == :mobile ? "diaspora_owncloud/application" : 
      "diaspora_owncloud/with_header" }

  respond_to :html,
             :mobile,
             :json

  def aspects
    aspect_ids = (session[:a_ids] || [])
    @stream = Stream::Aspect.new(current_user, aspect_ids,
                                 :max_time => max_time)
    stream_responder
  end

  def public
    stream_responder(Stream::Public)
  end

  def activity
    stream_responder(Stream::Activity)
  end

  def multi
      puts "%%%%%%% in multi"
      stream_responder(Stream::Multi)
  end

  def commented
    stream_responder(Stream::Comments)
  end

  def liked
    stream_responder(Stream::Likes)
  end

  def mentioned
    stream_responder(Stream::Mention)
  end

  def followed_tags
    gon.preloads[:tagFollowings] = tags
    stream_responder(Stream::FollowedTag)
  end

  private

  def stream_responder(stream_klass=nil)
    puts "%%%%%%% in stream responder"
    
    if stream_klass.present?
      @stream ||= stream_klass.new(current_user, :max_time => max_time)
    end
    
    ## For cookies when going to owncloud
    if (session[:owncloud_cookies] != nil)
      @use_owncloud_cookies = YAML.load(session[:owncloud_cookies])
      puts "%%%%%%% before cookie"
      puts "cookie "+@use_owncloud_cookies[0].name

      response.set_cookie(@use_owncloud_cookies[0].name, {:value => @use_owncloud_cookies[0].value, 
      :path => @use_owncloud_cookies[0].path, :domain => @use_owncloud_cookies[0].domain, 
      :created => @use_owncloud_cookies[0].created_at})
    end
    
    ##   ##############################

    respond_with do |format|
      format.html { render 'diaspora_owncloud_streams/main_stream' }
      format.mobile { render 'diaspora_owncloud_streams/main_stream' }
      format.json { render :json => @stream.stream_posts.map {|p| LastThreeCommentsDecorator.new(PostPresenter.new(p, current_user)) }}
    end
    
  end

  def save_selected_aspects
    if params[:a_ids].present?
      session[:a_ids] = params[:a_ids]
    end
  end
  
end
