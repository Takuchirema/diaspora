class DiasporaOwncloudRegistrationsController < ::RegistrationsController
  #layout "diaspora_owncloud/with_header"
  #include Devise::Controllers::Helpers
  include OwncloudHelper
  require 'net/http'
  require "uri"
  
  before_action :check_registrations_open_or_valid_invite!

  layout -> { request.format == :mobile ? "diaspora_owncloud/application" : "diaspora_owncloud/with_header" }
  
  def create
    
    set_session_variables
    
    @user = User.build(user_params)
    
    username = user_params["username"]
    password = user_params["password"]
    
    password_confirmation = user_params["password_confirmation"]
    
    failure_message = 'ownCloud server is down. Please try again later'
    if password.eql?password_confirmation
      uri = URI.parse(session[:owncloud_handle]+"ocs/v1.php/cloud/users")
      params = {'userid' => username , 'password'=>password}
      uri.query = URI.encode_www_form(params)

      http = Net::HTTP.new(uri.host, uri.port)
      request = Net::HTTP::Post.new(uri.request_uri)
      request.basic_auth(session[:owncloud_username],session[:owncloud_password])
      request.set_form_data(params)
      response = http.request(request)
      puts "%%%%%%%%% owncloud registration response "+response.body
    else
      failure_message = 'Password must match Password Confirmation'
    end
    
    begin
      data = Hash.from_xml(response.body)

      statuscode = data["ocs"]["meta"]["statuscode"]
      message = data["ocs"]["meta"]["message"]
      
      if (statuscode.eql? "100") || (statuscode.eql? "102")
        puts "%%%%%%% user is signed up on owncloud"
        set_owncloud_cookies(username,password)
        
        if @user.sign_up
          puts "%%%%%%% user is signed up"
          flash[:notice] = t("registrations.create.success")
          @user.process_invite_acceptence(invite) if invite.present?
          @user.seed_aspects
          @user.send_welcome_message
          sign_in_and_redirect(:user, @user)
          logger.info "event=registration status=successful user=#{@user.diaspora_handle}"
        else
          puts "%%%%%%% user is sign up failed diaspora"
          @user.errors.delete(:person)

          flash.now[:error] = @user.errors.full_messages.join(" - ")
          logger.info "event=registration status=failure errors='#{@user.errors.full_messages.join(', ')}'"
          render action: "new"
        end
        #render plain: data["ocs"]["meta"]["statuscode"]
      else
        puts "%%%%%%% user sign up failed owncloud"
        flash.now[:error] = "owncloud file sharing: "+message
        
        logger.info "event=registration status=failure errors='"+"owncloud file sharing: "+message+"'"
        render action: "new"
        #render plain: data
      end
    
    rescue => ex
      flash.now[:error] = failure_message
        
      logger.info "event=registration status=failure errors='"+failure_message+"'"
      render action: "new"
    end
    
  end
  
  private

  def check_registrations_open_or_valid_invite!
    return true if AppConfig.settings.enable_registrations? || invite.try(:can_be_used?)

    flash[:error] = params[:invite] ? t("registrations.invalid_invite") : t("registrations.closed")
    redirect_to new_user_session_path
  end

  def invite
    @invite ||= InvitationCode.find_by_token(params[:invite][:token]) if params[:invite].present?
  end

  helper_method :invite

  def user_params
    params.require(:user).permit(
      :username, :email, :getting_started, :password, :password_confirmation, :language, :disable_mail,
      :show_community_spotlight_in_stream, :auto_follow_back, :auto_follow_back_aspect_id,
      :remember_me, :captcha, :captcha_key
    )
  end
  
end
