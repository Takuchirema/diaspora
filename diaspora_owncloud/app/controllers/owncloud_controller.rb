require "diaspora_owncloud_sessions_controller"

class OwncloudController < ApplicationController
  $local_owncloud_cookies
  $local_dav
  
  def show
    
    begin
      @use_owncloud_cookies = YAML.load(session[:owncloud_cookies])

      puts "%%%%%%% before cookie"
      puts "cookie "+@use_owncloud_cookies.inspect
      puts "name "+@use_owncloud_cookies[0].name
      puts "value "+@use_owncloud_cookies[0].value

      response.set_cookie(@use_owncloud_cookies[0].name, {:value => @use_owncloud_cookies[0].value, 
      :path => @use_owncloud_cookies[0].path, :domain => @use_owncloud_cookies[0].domain, 
      :created => @use_owncloud_cookies[0].created_at, :httponly=>false})
      redirect_to session[:owncloud_handle]
      #redirect_to(:back)
    rescue
      flash[:error] = "owncloud : Server is off please try again later"
      redirect_to(:back)
    end
   
  end
  
  def webdav
    require 'rubygems'
    require 'net/dav'
    
    @files = Array.new
    
    puts '%%%%%% in webdav'
    
    @use_dav = YAML.load(session[:dav])

    @use_dav.find('.',:recursive=>true,:suppress_errors=>false,:filename=>/\.*$/) do | item |
      puts " %%%%%% Checking: " + item.url.to_s
      @files.push(item.url.to_s)
    end
    
    if request.xhr?
      render :json => @files
    else
      puts request.inspect
      render "show"
    end
    
  end
  
  def file
    require "base64"
    require 'open-uri'
    require 'rubygems'
    require 'net/dav'
    require 'json'
    
    $local_dav = YAML.load(session[:dav])
    
    file_path = params[:file_name]
    puts '%%%%%% in file '+file_path
    file_path = file_path.sub(' ', '%20')
    
    dav_hash = JSON.parse($local_dav.to_json)
    puts '%%%%%% in file '+file_path
    
    if file_path.include? "/"
      @file_name = params[:file_name][params[:file_name].rindex('/')+1,params[:file_name].length]
    else
      @file_name = file_path
    end
    
    
    path = session[:owncloud_handle]+"remote.php/webdav/"+file_path
    
    uri = URI.parse(path)

    http = Net::HTTP.new(uri.host, uri.port)
    file_request = Net::HTTP::Get.new(uri.request_uri)
    file_request.basic_auth(dav_hash['handler']['user'], dav_hash['handler']['pass'])
    response = http.request(file_request)
    
    encoded = Base64.encode64(response.body)
    
    if request.xhr?
      puts 'in xhr'
      send_data encoded, :type => "image/jpeg", :disposition => "inline"
    else
      puts request.inspect
      render "show"
    end
   
  end
    
end
