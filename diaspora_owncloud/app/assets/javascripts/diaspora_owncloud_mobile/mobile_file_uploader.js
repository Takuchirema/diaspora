// @license magnet:?xt=urn:btih:0b31508aeb0634b347b8270c7bee4d411b5d4109&dn=agpl-3.0.txt AGPL-v3-or-Later
//= require js_image_paths

function createUploader(){
   var aspectIds = gon.preloads.aspect_ids;

   new qq.FileUploaderBasic({
       element: document.getElementById('file-upload-publisher'),
       params: {'photo' : {'pending' : 'true', 'aspect_ids' : aspectIds},},
       allowedExtensions: ['jpg', 'jpeg', 'png', 'gif', 'tiff'],
       action: "/diaspora_owncloud_photos",
       debug: false,
       button: document.getElementById('file-upload-publisher'),
       sizeLimit: 4194304,

       onProgress: function(id, fileName, loaded, total){
        var progress = Math.round(loaded / total * 100 );
         $('#fileInfo-publisher').text(fileName + ' ' + progress + '%');
       },

       messages: {
          typeError: Diaspora.I18n.t("photo_uploader.invalid_ext"),
          sizeError: Diaspora.I18n.t("photo_uploader.size_error"),
          emptyError: Diaspora.I18n.t("photo_uploader.empty")
       },

       onSubmit: function(){
        $('#file-upload-publisher').addClass("loading");
        $('#publisher_textarea_wrapper').addClass("with_attachments");
        $('#photodropzone').append(
          "<li class='publisher_photo loading' style='position:relative;'>" +
            "<img alt='Ajax-loader2' src='"+ImagePaths.get('ajax-loader2.gif')+"' />" +
          "</li>"
          );
       },

       onComplete: function(_id, fileName, responseJSON) {
        $('#fileInfo-publisher').text(Diaspora.I18n.t("photo_uploader.completed", {'file': fileName}));
        var id = responseJSON.data.photo.id,
            url = responseJSON.data.photo.unprocessed_image.url,
            currentPlaceholder = $('li.loading').first();

        $('#publisher_textarea_wrapper').addClass("with_attachments");
        $('#new_status_message').append("<input type='hidden' value='" + id + "' name='photos[]' />");

        // replace image placeholders
        var img = currentPlaceholder.find('img');
        img.attr('src', url);
        img.attr('data-id', id);
        currentPlaceholder.removeClass('loading');
        currentPlaceholder.append("<div class='x'>X</div>" +
            "<div class='circle'></div>");
        ////

        var publisher = $('#publisher');

        publisher.find("input[type='submit']").removeAttr('disabled');

        $('.x').bind('click', function(){
          var photo = $(this).closest('.publisher_photo');
          photo.addClass("dim");
          $.ajax({url: "/photos/" + photo.children('img').attr('data-id'),
                  dataType: 'json',
                  type: 'DELETE',
                  success: function() {
                            photo.fadeOut(400, function(){
                              photo.remove();
                              if ( $('.publisher_photo').length === 0){
                                $('#publisher_textarea_wrapper').removeClass("with_attachments");
                              }
                            });
                          }
                  });
        });
       },

       onAllComplete: function(){}
   });
}

function createOwncloudUploader(){
    
    $("#jstree-container").on('keyup',"input", function(e) {
        var searchString = $(this).val();
        //alert ('on key up '+searchString);
        console.log(searchString);
        $('#owncloud-jstree').jstree('search', searchString);
    });
    
    $("#owncloud-upload-mobile").on("click", function(event){
        if ($('#jstree-container').is(':empty')){
            //alert('empty');
            document.getElementById("jstree-container").style.display = "block";
            document.getElementById("jstree-container").innerHTML = 
                    "<div class=\"jstree-search-input\">\n\
                        <input placeholder=\"search your ownCloud files\" id=\"search-input\"></input>\n\
                    </div>"+
                    "<div id=\"owncloud-jstree\"></div>";
            owncloud_jstree();
        }else{
            //alert('not empty');
            document.getElementById("jstree-container").style.display = "none";
            document.getElementById("jstree-container").innerHTML = '';
        }
        
    });
}

function owncloud_jstree(){
    //alert ("owncloud clicked");
    //var query = $('#search').value();
    var uri = "/webdav";
    var self = this;
    
    $.get(uri, function(data) {
        //console.log(data[0]);
        var hierarchy = data.reduce(function(hier,path){
            var x = hier;
            var from = path.indexOf("webdav")+7 ;
            path = path.substr(from,path.length);
            
            path.split('/').forEach(function(item){
                if(!x[item]){
                    x[item] = {};
                }
                x = x[item];
            });
            x.path = path;
            return hier;
        }, {});
        
        var makeul = function(hierarchy, classname){
            var dirs = Object.keys(hierarchy);
            var ul = '<ul';
            if(classname){
                ul += ' class="' + classname + '"';
            }
            ul += '>\n';
            dirs.forEach(function(dir){
                var path = hierarchy[dir].path;
                if(path){ // file
                    var dot = path.indexOf(".");
                    var extension = path.substr(dot,path.length);
                    
                    dir = dir.replace("%20", " ");
                    if (extension.match(/\.(JPG|jpg|JPEG|jpeg|png|PNG|GIF|gif)$/)){
                        ul += '<li class="image" data-url="' + path + '">' + dir + '</li>\n';
                    }else{
                        ul += '<li class="file" data-url="' + path + '">' + dir + '</li>\n';
                    }
                }else{
                    ul += '<li class="folder">' + dir + '\n';
                    ul += makeul(hierarchy[dir]);
                    ul += '</li>\n';
                }
                
            });
            ul += '</ul>\n';
            return ul;
        };
        
        var ul = makeul(hierarchy, 'base-UL');
        
        document.getElementById("owncloud-jstree").innerHTML = ul;
        
        $('#owncloud-jstree').on('select_node.jstree', function (e, data) {
            
            //alert ("node clicked");
            var node_id   = (data.node.id); // element id
            var url = $("#"+node_id).attr("data-url");
            
            if (url != null){
                var dot = url.indexOf(".");
                var extension = url.substr(dot,url.length);
                    
                if (!extension.match(/\.(JPG|jpg|JPEG|jpeg|png|PNG|GIF|gif)$/)){
                    var file_name = url.substr(url.lastIndexOf("/")+1,url.length);
                    file_name = file_name.replace("%20", " ");
                    alert (file_name+" has invalid extensions. Only jpg, png, gif and tiff are allowed");
                }else{
                    
                
                    console.log(url);
                    var uri = "/webdav/file?file_name="+url;
                    var filename = url.substr(url.lastIndexOf('/')+1,url.length);

                    $.get(uri, function(data) {
                        //console.log(data);
                        //alert ('got data '+typeof(data));

                        var image = new Image();
                        image.src = 'data:image/jpg;base64,'+data;

                        var file = dataURLtoFile('data:image/jpg;base64,'+data,filename);
                        console.log(file);

                        ownCloudUploader(file);

                    });
                }
            }
            
        }).jstree({
            "types" : {
                "root" : {
                    "icon" : "../..images/favicon.png",
                    "valid_children" : ["default"]
                },
                "default" : {
                    "valid_children" : ["default","file"]
                },
                "file" : {
                    "icon" : "glyphicon glyphicon-file",
                    "valid_children" : []
                }
            },
            
            "search": {
                "case_insensitive": true,
                "show_only_matches" : true
            },
            
            "plugins" : [
                "contextmenu", "dnd", "search",
                "state", "types", "wholerow"
            ]
        });
        
    });
    
}
  
function dataURLtoFile(dataurl, filename) {
    var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
            bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
    while(n--){
        u8arr[n] = bstr.charCodeAt(n);
    }
    
    return new File([u8arr], filename, {type:mime});
    
}

function ownCloudUploader(file){
                    
   var aspectIds = gon.preloads.aspect_ids;
   
   new qq.OwncloudFileUploaderBasic({
       file: file,
       params: {'photo' : {'pending' : 'true', 'aspect_ids' : aspectIds},},
       allowedExtensions: ['jpg', 'jpeg', 'png', 'gif', 'tiff'],
       action: "/photos",
       debug: false,
       sizeLimit: 4194304,

       onProgress: function(id, fileName, loaded, total){
        var progress = Math.round(loaded / total * 100 );
         $('#fileInfo-publisher').text(fileName + ' ' + progress + '%');
       },

       messages: {
          typeError: Diaspora.I18n.t("photo_uploader.invalid_ext"),
          sizeError: Diaspora.I18n.t("photo_uploader.size_error"),
          emptyError: Diaspora.I18n.t("photo_uploader.empty")
       },

       onSubmit: function(){
        $('#file-upload-publisher').addClass("loading");
        $('#publisher_textarea_wrapper').addClass("with_attachments");
        $('#photodropzone').append(
          "<li class='publisher_photo loading' style='position:relative;'>" +
            "<img alt='Ajax-loader2' src='"+ImagePaths.get('ajax-loader2.gif')+"' />" +
          "</li>"
          );
       },

       onComplete: function(_id, fileName, responseJSON) {
        $('#fileInfo-publisher').text(Diaspora.I18n.t("photo_uploader.completed", {'file': fileName}));
        var id = responseJSON.data.photo.id,
            url = responseJSON.data.photo.unprocessed_image.url,
            currentPlaceholder = $('li.loading').first();

        $('#publisher_textarea_wrapper').addClass("with_attachments");
        $('#new_status_message').append("<input type='hidden' value='" + id + "' name='photos[]' />");

        // replace image placeholders
        var img = currentPlaceholder.find('img');
        img.attr('src', url);
        img.attr('data-id', id);
        currentPlaceholder.removeClass('loading');
        currentPlaceholder.append("<div class='x'>X</div>" +
            "<div class='circle'></div>");
        ////

        var publisher = $('#publisher');

        publisher.find("input[type='submit']").removeAttr('disabled');

        $('.x').bind('click', function(){
          var photo = $(this).closest('.publisher_photo');
          photo.addClass("dim");
          $.ajax({url: "/photos/" + photo.children('img').attr('data-id'),
                  dataType: 'json',
                  type: 'DELETE',
                  success: function() {
                            photo.fadeOut(400, function(){
                              photo.remove();
                              if ( $('.publisher_photo').length === 0){
                                $('#publisher_textarea_wrapper').removeClass("with_attachments");
                              }
                            });
                          }
                  });
        });
       },

       onAllComplete: function(){}
   });
    
}

window.addEventListener("load", function() {
  createUploader();
  createOwncloudUploader();
});
// @license-end
