// @license magnet:?xt=urn:btih:0b31508aeb0634b347b8270c7bee4d411b5d4109&dn=agpl-3.0.txt AGPL-v3-or-Later

/*   Copyright (c) 2010-2011, Diaspora Inc.  This file is
 *   licensed under the Affero General Public License version 3 or later.  See
 *   the COPYRIGHT file.
 */
//= require jquery-textchange
//= require charcount
//= require js-routes
//= require autosize
//= require keycodes
//= require jquery.autoSuggest.custom
//= require fileuploader-custom
//= require rails-timeago
//= require underscore
//= require bootstrap
//= require diaspora
//= require helpers/i18n
//= require widgets/timeago
//= require diaspora_owncloud_mobile/mobile_application
//= require diaspora_owncloud_mobile/mobile_file_uploader
//= require diaspora_owncloud_mobile/profile_aspects
//= require diaspora_owncloud_mobile/tag_following
//= require diaspora_owncloud_mobile/publisher
//= require diaspora_owncloud_mobile/mobile_comments
//= require diaspora_owncloud_mobile/mobile_post_actions
//= require diaspora_owncloud_mobile/mobile_drawer
//= require diaspora_owncloud_mobile/diaspora_owncloud_fileuploader
//= require_tree ../dist
// @license-end
