Rails.application.routes.draw do
  
  resources :diaspora_owncloud_status_messages, :only => [:new, :create]
  
  # Streams
  get "activity" => "diaspora_owncloud_streams#activity", :as => "activity_stream"
  get "stream" => "diaspora_owncloud_streams#multi", :as => "stream"
  get "public" => "diaspora_owncloud_streams#public", :as => "public_stream"
  get "followed_tags" => "diaspora_owncloud_streams#followed_tags", :as => "followed_tags_stream"
  get "mentions" => "diaspora_owncloud_streams#mentioned", :as => "mentioned_stream"
  get "liked" => "diaspora_owncloud_streams#liked", :as => "liked_stream"
  get "commented" => "diaspora_owncloud_streams#commented", :as => "commented_stream"
  get "aspects" => "diaspora_owncloud_streams#aspects", :as => "aspects_stream"
  
  resources :diaspora_owncloud_photos, only: %i(destroy create) do
    put :make_profile_photo
  end
  
  resources :diaspora_owncloud_notifications, :only => [:index, :update] do
    collection do
      get :read_all
    end
  end
  
  resource :diaspora_owncloud_user, only: %i(edit destroy), shallow: true do
    put :edit, action: :update
    get :getting_started_completed
    post :export_profile
    get :download_profile
    post :export_photos
    get :download_photos
  end
  
  devise_for :users, controllers: {sessions: "diaspora_owncloud_sessions",
    :omniauth_callbacks => "omniauth_callbacks"}, 
    skip: :registration
    
  devise_scope :user do
    get "/users/sign_up" => "diaspora_owncloud_registrations#new",    :as => :new_user_registration
    post "/users"        => "diaspora_owncloud_registrations#create", :as => :user_registration
  end
  
  get 'mobile/toggle', :to => 'diaspora_owncloud_home#toggle_mobile', :as => 'toggle_mobile'
  get "/m", to: "diaspora_owncloud_home#force_mobile", as: "force_mobile"
  
  # Startpage
  root :to => 'diaspora_owncloud_home#show'
  get "podmin", to: "diaspora_owncloud_home#podmin"
  
  #diaspora_owncloud
  get "owncloud", to: "owncloud#show"
  
  get "webdav",to: "owncloud#webdav"
  
  get "webdav/file",to: "owncloud#file"
  
end
